import java.text.SimpleDateFormat;
import java.util.Date;

public class DateProvider {
    public String now() {
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }
}

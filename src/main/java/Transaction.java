public class Transaction {
    private float depot, retrait;
    private float balanceActuelle = 0f;

    public Transaction(){
        this.depot = 0f;
        this.retrait = 0f;
    }

    public Transaction(float depot, float retrait){
        this.depot = depot;
        this.retrait = retrait;
    }

    public void setBalanceActuelle(float balanceActuelle) {
        this.balanceActuelle = balanceActuelle;
    }

    public float getBalanceActuelle() {
        return balanceActuelle;
    }
}


public class Main {
    public static void main(String[] args){
        Compte compte = new Compte(new DateProvider(), new TransferArgentApi());
        compte.depot(1000f);
        compte.retrait(19.90f);

        System.out.println("Date   Transaction   Balance");

        for(String item: compte.afficherHistoriqueTransaction()){
            System.out.println(item);
        }
    }
}
